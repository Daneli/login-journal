import { authSlice, login, logout } from "../src/store/auth/authSlice";
import {
  authenticatedState,
  demoUser,
  initialState,
} from "./fixtures/authFixtures";

describe("Testing authSlice", () => {
  test("Must return initial state and it will call 'auth'", () => {
    const state = authSlice.reducer(initialState, {});

    expect(authSlice.name).toBe("auth");
    expect(state).toEqual(initialState);
  });

  test("Must realice authentication", () => {
    const state = authSlice.reducer(initialState, login(demoUser));

    expect(state).toEqual({
      status: "authenticated",
      uid: demoUser.uid,
      email: demoUser.email,
      displayName: demoUser.displayName,
      photoURL: demoUser.photoURL,
      errorMessage: null,
    });
  });

  test("Must realice logout", () => {
    const state = authSlice.reducer(authenticatedState, logout());
    expect(state).toEqual({
      status: "not-authenticated",
      uid: null,
      email: null,
      displayName: null,
      photoURL: null,
      errorMessage: undefined,
    });
  });

  test("Must realice logout and show message error", () => {
    const errorMessage = "Credenciales no son correctas";

    const state = authSlice.reducer(authenticatedState, logout());
    expect(state).toEqual({
      status: "not-authenticated",
      uid: null,
      email: null,
      displayName: null,
      photoURL: null,
      errorMessage: errorMessage,
    });
  });

  test('Must change state to "checking"', () => {
    const state = authSlice.reducer(authenticatedState, logout());
    expect(state.state).toBe("checking");
  });
});
