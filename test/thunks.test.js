import {
  LoginWithEmailPassword,
  logoutFirebase,
} from "../src/firebase/providers";
import {
  checkingCredentials,
  login,
  logout,
} from "../src/store/auth/authSlice";
import {
  checkingAuthentication,
  startGoogleSignIn,
  startLoginWithEmailPassword,
  startLogout,
} from "../src/store/auth/thunk";
import { demoUser } from "./fixtures/authFixtures";

jest.mock("../src/store/auth/thunk");

describe("Testing thunks", () => {
  const dispatch = jest.fn();
  beforeEach(() => jest.clearAllMocks());

  test("Must take the chekingCredentials", async () => {
    await checkingAuthentication()(dispatch);

    expect(dispatch).toHaveBeenNthCalledWith(checkingCredentials());
  });

  test("Must startGoogleSignIn call checkingCredentials and login - success", async () => {
    const loginData = { ok: true, ...demoUser };
    await signInWithGoogle.mockResolvedValue(loginData);

    await startGoogleSignIn()(dispatch);

    expect(dispatch).toHaveBeenCalledWith(checkingCredentials());
    expect(dispatch).toHaveBeenCalledWith(login(loginData));
  });

  test("Must startGoogleSignIn call checkingCredentials and login - error", async () => {
    const loginData = { ok: false, errorMessage: "Error" };
    await signInWithGoogle.mockResolvedValue(loginData);

    await startGoogleSignIn()(dispatch);

    expect(dispatch).toHaveBeenCalledWith(checkingCredentials());
    expect(dispatch).toHaveBeenCalledWith(login(loginData.errorMessage));
  });

  test("startLoginWithEmailPassword must call checkingCredentials and login - success", async () => {
    const loginData = { ok: true, ...demoUser };
    const formData = { email: demoUser.email, password: "12345678" };

    await LoginWithEmailPassword.mockResolvedValue(loginData);

    await startLoginWithEmailPassword(formData)(dispatch);

    expect(dispatch).toHaveBeenCalledWith(checkingCredentials());
    expect(dispatch).toHaveBeenCalledWith(login(loginData));
  });

  test("startLogout must call logoutFirebase, clearNotes and logout", async () => {
    await startLogout();

    expect(logoutFirebase).toHaveBeenCalled();
    expect(dispatch).toHaveBeenCalledWith(clearNotesLogout());
    expect(dispatch).toHaveBeenCalledWith(logout());
  });
});
