import { fileUpload } from "../src/helpers/fileYpload";
import { v2 as cloudinary } from "cloudinary";

cloudinary.config({
  cloud_name: "dsj6o0p7r",
  api_key: "885993283212256",
  api_secret: "RJBCnPWsJmqO1UnM2ovoh31m6dU",
  secure: true,
});

describe("Testing fileUpload", () => {
  test("Must update file correctly to Cloudinary", async () => {
    const imageUrl =
      "https://www.lavanguardia.com/files/content_image_mobile_filter/uploads/2021/07/20/60f6c971bc94a.jpeg";
    const resp = await fetch(imageUrl);

    const blob = await resp.blob();
    const file = new File([blob], "foto.jpg");
    const url = await fileUpload(file);

    expect(typeof url).toBe("string");

    const segments = url.split("/");
    const imageId = segments[segments.length - 1].replace(".jpg", "");

    const cloudResp = await cloudinary.api.delete_resources([
      "journal/" + imageId,
      {
        resource_type: "image",
      },
    ]);
  });

  test("Must return NULL", async () => {
    const file = new File([blob], "foto.jpg");
    const url = await fileUpload(file);

    expect(url).toBe(null);
  });
});
