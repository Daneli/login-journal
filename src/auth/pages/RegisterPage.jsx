import { Alert, Button, Grid, Link, TextField, Typography } from "@mui/material"
import AuthLayout from "../layout/AuthLayout"
import { Link as RouterLink } from "react-router-dom"
import { useForm } from "../../hooks/useForm"
import { useMemo, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { startCreatingUserEmailPassword } from "../../store/auth/thunk"

const formData =
{
    email: '',
    password: '',
    displayName: ''
}

const formValidations = {
    email: [(value) => value.includes('@'), 'El correo debe tener un @'],
    password: [(value) => value.length >= 6, 'El password debe de tener mas de 6 letras'],
    displayName: [(value) => value.length >= 1, 'El nombre es requerido']
}

const RegisterPage = () => {

    const dispatch = useDispatch()

    const [formSubmitted, setFormSubmitted] = useState(false)
    const { status, errorMessage } = useSelector(state => state.auth)
    const isCheckingAuthentication = useMemo(() => status === 'checking', [status])

    const { formState, displayName, email, password,
        onInputChange, isFormValid, displayNameValid,
        emailValid, passwordValid } = useForm(formData, formValidations)

    const onSubmit = (e) => {
        e.preventDefault()
        setFormSubmitted(true)
        if (isFormValid) return
        dispatch(startCreatingUserEmailPassword(formState))
    }

    return (
        <>
            <AuthLayout title="Crear cuenta">
                <h1>FormValid {isFormValid ? 'valido' : 'Incorrecto'}</h1>
                <form onSubmit={onSubmit}>
                    <Grid container>
                        <Grid item xs={12} sx={{ mt: 2 }}>
                            <TextField
                                label="Nombre completo"
                                type="text"
                                placeholder="Nombre completo"
                                name="displayName"
                                value={displayName}
                                onChange={onInputChange}
                                error={!!displayNameValid && formSubmitted}
                                helperText={displayNameValid}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={12} sx={{ mt: 2 }}>
                            <TextField
                                label="Correo"
                                type="email"
                                placeholder="correo@gmail.com"
                                name="email"
                                value={email}
                                onChange={onInputChange}
                                error={!!emailValid && formSubmitted}
                                helperText={emailValid}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={12} sx={{ mt: 2 }}>
                            <TextField
                                label="Contraseña"
                                type="password"
                                placeholder="Contraseña"
                                name="password"
                                value={password}
                                onChange={onInputChange}
                                error={!!passwordValid && formSubmitted}
                                helperText={passwordValid}
                                fullWidth
                            />
                        </Grid>
                        <Grid container spacing={2} sx={{ mb: 2, mt: 4 }}>

                            <Grid item xs={12} display={errorMessage ? 'none' : ''}>
                                <Alert severity="error">
                                    {errorMessage}
                                </Alert>
                            </Grid>
                            <Grid item xs={12}>
                                <Button type="submit" variant="contained" disabled={isCheckingAuthentication} fullWidth>
                                    Crear cuenta
                                </Button>
                            </Grid>
                            <Grid container direction="row" justifyContent="end">
                                <Typography sx={{ mr: 1 }}>¿Ya tienes cuenta?</Typography>
                                <Link component={RouterLink} color="inherit" to="/auth/login">
                                    Ingresar
                                </Link>
                            </Grid>
                        </Grid>
                    </Grid>
                </form>
            </AuthLayout>
        </>
    );
}

export default RegisterPage