import { Route, Routes } from "react-router-dom"
import AuthRoutes from "../auth/routes/AuthRoutes"
import JournalRoutes from "../journal/routes/JournalRoutes"
//import useCheckAuth from "../hooks/useCheckAuth"

const AppRoutes = () => {

    //const { status } = useCheckAuth()

    return (
        <>
            <Routes>
                {/* {
                    (status === 'authenticated'
                        ?  */}
                <Route path="/auth/*" element={<AuthRoutes />} />
                {/* :  */}
                <Route path="/*" element={<JournalRoutes />} />
                {/* )
                } */}
            </Routes>
        </>

    )
}

export default AppRoutes