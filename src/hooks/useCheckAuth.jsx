import { onAuthStateChanged } from "firebase/auth";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { login, logout } from "../store/auth/authSlice.js";
import { FirebaseAuth } from "../firebase/config.js";
import CheckingAuth from "../ui/components/CheckingAuth.jsx";
import { startLoadingNotes } from "../store/journal/thunks.js";

const useCheckAuth = () => {
  const { status } = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  useEffect(() => {
    onAuthStateChanged(FirebaseAuth, async (user) => {
      if (!user) return dispatch(logout());

      const { uid, email, displayName, photoURL } = user;
      dispatch(login({ uid, email, displayName, photoURL }));
      dispatch(login(startLoadingNotes()));
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (status === "checking") {
    return <CheckingAuth />;
  }
};

export default useCheckAuth;
