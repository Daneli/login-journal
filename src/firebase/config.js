// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore/lite";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCNfH51zHEVxSORToAaGnZ8i0KYcrq5BIU",
  authDomain: "react-cursos-93177.firebaseapp.com",
  projectId: "react-cursos-93177",
  storageBucket: "react-cursos-93177.appspot.com",
  messagingSenderId: "501413966914",
  appId: "1:501413966914:web:b856978a0ea374f86f29db",
};

// Initialize Firebase
export const FirebaseApp = initializeApp(firebaseConfig);
export const FirebaseAuth = getAuth(FirebaseApp);
export const FirebaseDB = getFirestore(FirebaseApp);
