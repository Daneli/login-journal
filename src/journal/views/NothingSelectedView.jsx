import { StarBorderOutlined } from "@mui/icons-material"
import { Grid, Typography } from "@mui/material"

const NothingSelectedView = () => {
    return (
        <Grid
            className="animate__animated animate__fadeIn animate__faster"
            container
            spacing={0}
            direction="column"
            alignItems="center"
            justifyContent="center"
            sx={{ minHeight: '93vh', backgroundColor: 'primary.main', borderRadius: 5 }}
        >
            <Grid item xs={12}>
                <StarBorderOutlined sx={{ fontSize: 100, color: 'white' }} />
            </Grid>
            <Grid item xs={12}>
                <Typography color="white" variant="h5">Selecciona o crea una entrada</Typography>
            </Grid>
        </Grid>

    )
}

export default NothingSelectedView