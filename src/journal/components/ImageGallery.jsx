import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';

export default function ImageGallery(images) {
    return (
        <ImageList sx={{ width: '100%', height: 500 }} cols={3} rowHeight={200}>
            {images.map((image) => (
                <ImageListItem key={image.img}>
                    <img
                        srcSet={`${image.img}?w=164&h=164&fit=crop&auto=format&dpr=2 2x`}
                        src={`${image.img}?w=164&h=164&fit=crop&auto=format`}
                        alt='Imagen de la nota'
                        loading="lazy"
                    />
                </ImageListItem>
            ))}
        </ImageList>
    );
}