import { Box } from "@mui/material"
import Navbar from "../components/Navbar"
//import SideBar from "../components/SideBar";

const drawerWidth = 240;

// eslint-disable-next-line react/prop-types
const JournalLayout = ({ children }) => {
    return (
        <Box sx={{ display: 'flex' }} className="animate__animated animate__fadeIn animate__faster">
            <Navbar drawerWidth={drawerWidth} />

            <Box component='main' sx={{ flexGrow: 1, p: 3 }}>
                {children}
            </Box>
        </Box>
    )
}

export default JournalLayout